class SessionController < ApplicationController

  def login
  end

  def authenticate_user
    username = params[:username]
    password = params[:password]
    if password == "aaa"
      session[:user] = params[:username]
      flash[:notice] = "Logged in successfully!"
      redirect_to employees_path
    else
      flash[:notice] = "Incorrect password!"
      redirect_to session_login_path
    end

  end

  def logout
    session[:user] = nil
    redirect_to session_login_path
  end
end
